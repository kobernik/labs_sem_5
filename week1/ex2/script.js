let count = prompt('Введите кол-во переменных:');
let counts = []

for (let i = 0; i < count; i++) {
  counts[i] = prompt('Введите переменную:');
}

for (let i = 0; i < count; i++) {
  alert(`Переменная ${i + 1}: ${typeof counts[i]}`);
}

//если все типы переменных числовые, то выведет минимальное и максимальное
//значение из массива данных, иначе выведет NaN
alert(Math.min.apply(null, counts));
alert(Math.max.apply(null, counts));
