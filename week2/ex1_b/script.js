function month_func() {
  const month_const = document.getElementById('month_const').value;

  if (Number(month_const) > 1 || Number(month_const) < 12) {
    switch(Number(month_const)) {
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      case 12:
        document.getElementById("txt").innerHTML = "В " + month_const + " месяце 31 день";
        break

      case 2:
        document.getElementById("txt").innerHTML = "В " + month_const + " месяце 28 дней (29 в високосном)";
        break

      case 4:
      case 6:
      case 9:
      case 11:
        document.getElementById("txt").innerHTML = "В " + month_const + " месяце 31 день";
        break
      default:
        document.getElementById("txt").innerHTML = "Введен несуществующий номер месяца";
        break

    }
  } 
}
